#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
import logging
import json

from instabot_py import InstaBot
from multiprocessing import Processå

config_location = "./instabot.config.ini"
config = configparser.ConfigParser()
config.read(config_location)
bots = {}

def getuserconfig(user_name):
    configdict = dict(config.items(user_name))

    for _setting, _value in configdict.items():

        try:
            if "{" in _value or "[" in _value:
                json.loads(_value)
                configdict[_setting] = json.loads(_value)
            else:
                raise ValueError
        except ValueError:
            if _value.isdigit() is True:
                configdict[_setting] = int(_value)
            if _value.lower == "true":
                configdict[_setting] = True
            if _value.lower == "false":
                configdict[_setting] = False
            if _value.lower == "none":
                configdict[_setting] = None
            pass

    configdict["login"] = configdict.pop("username")

    return configdict


for userConfig in config.sections():
    bots[userConfig] = InstaBot(**getuserconfig(userConfig))
    print (userConfig, getuserconfig(userConfig))

if __name__ == '__main__':
    for user_bot in bots:
        Process(target=bots[user_bot].mainloop()).start()
