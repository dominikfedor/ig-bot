#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
import logging
import json
import sys

from instabot_py import InstaBot

config_location = "instabot/config_modified.ini"
config = configparser.ConfigParser()
config.read(config_location)
bots = {}
user = sys.argv[1]

def getuserconfig():
    configdict = dict(config.items(user))

    for _setting, _value in configdict.items():

        try:
            if "{" in _value or "[" in _value:
                json.loads(_value)
                configdict[_setting] = json.loads(_value)
            else:
                raise ValueError
        except ValueError:
            if _value.isdigit() is True:
                configdict[_setting] = int(_value)
            if _value.lower == "true":
                configdict[_setting] = True
            if _value.lower == "false":
                configdict[_setting] = False
            if _value.lower == "none":
                configdict[_setting] = None
            pass

    configdict["login"] = configdict.pop("username")

    return configdict

bot = InstaBot(**getuserconfig())

if __name__ == '__main__':
  bot.mainloop()
