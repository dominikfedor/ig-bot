// @flow
import fs from 'fs';
import fsExtra from 'fs-extra';
import * as _ from 'lodash';
import ini from 'ini';
import csv from 'csv-parser';

import React, { Component } from 'react';
import { PythonShell } from 'python-shell';

import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import CheckedIcon from '@material-ui/icons/CheckRounded';
import CloseIcon from '@material-ui/icons/close'
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input'

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';

import TextField from '@material-ui/core/TextField';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';

import styles from './Home.css';
import routes from '../constants/routes';

interface HomeState {
  activeUser: object;
  started: object;
  loading: boolean;
  list: object;
  editing: boolean;
  loginPassword: string;
  loginUser: string;
  dialog: 'login' | 'historyLog' | '' | 'deleteConfirm' | 'follow_list' | 'unfollow_whitelist';
  config: any,
}

const configFilePath = './instabot/config_modified.ini';
const botScriptPath = 'bot_main.py';
const configFile = ini.parse(fs.readFileSync(configFilePath, 'utf-8'));

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeUser: {},
      started: {},
      loginUser: '',
      loginPassword: '',
      config: null,
      listPreview: { list: [], path: "" },
      loading: false,
      dialog: '',
    };
  }

  componentDidMount() {
    const activeUser = _.first(_.filter(configFile, 'username'));
    this.setState({
      activeUser: { ...configFile.DEFAULT, ...activeUser },
      config: configFile,
      started: { [_.get(activeUser, "username", "")]: false }
    });
  }

  botShell = {};
  filesInput = null


  handleConfigSave = async () => {
    const { activeUser, config } = this.state;
    await fs.writeFileSync(
      configFilePath,
      ini.encode({ ...config, [activeUser.username]: activeUser })
    );
  };

  updateConfig = () => {
    this.setState({
      config: ini.parse(fs.readFileSync(configFilePath, 'utf-8'))
    });
  };

  handleStart = async () => {
    await this.handleConfigSave()
    const {
      activeUser: { username },
      started
    } = this.state;
    const options = {
      scriptPath: 'instabot/',
      args: [username.replace(/\./g, '\\.')]
    };
    const shell = new PythonShell(botScriptPath, options);
    // const shell = PythonShell.runString('instaloader --version', null, function (err) {
    //   if (err) throw err;
    //   console.log('finished');
    // });

    shell.on('message', msg => {
      console.log('MSG', username, msg)
      fs.appendFileSync(`${username}.log`, `${msg}'\r\n'`)
    })
    shell.on('stdout', msg => console.log('STDOUT', username, msg));
    shell.on('stderr', msg => console.log('STDERR', username, msg));

    this.botShell[username] = shell;
    this.setState({ started: { ...started, [username]: true } });
  };

  handleAddUser = async () => {
    const { loginPassword, loginUser, config } = this.state;
    const activeUser = {
      ...config.DEFAULT,
      username: loginUser,
      password: loginPassword
    };

    await fs.writeFileSync(
      configFilePath,
      ini.encode({ ...config, [activeUser.username]: activeUser })
    );

    this.setState({
      activeUser,
      config: { ...config, [activeUser.username]: activeUser },
      dialog: '',
      loginUser: 'adsf',
      loginPassword: 'asdf'
    });
  };

  handleRemoveUser = async () => {
    const {
      config,
      activeUser: { username }
    } = this.state;

    config[username] = {};

    await fs.writeFileSync(configFilePath, ini.encode(config));

    this.setState({ config, dialog: '', activeUser: {} });
  };

  handleStop = async () => {
    const {
      started,
      activeUser: { username }
    } = this.state;

    await this.botShell[username].end();
    this.botShell[username].terminate('SIGINT');
    this.botShell[username].terminate('SIGKILL');

    this.setState({ started: { ...started, [username]: false } });
  };

  handleStartTimeChange = ({ target: { value } }) => {
    const {
      activeUser
    } = this.state;
    const time = value.split(":")
    console.log(time)
    activeUser.start_at_h = time[0] || "None";
    activeUser.start_at_m = time[1] || "None";
    this.setState({ activeUser });
  };

  handleEndTimeChange = ({ target: { value } }) => {
    const { activeUser } = this.state

    const time = value.split(":")
    activeUser.end_at_h = time[0] || "None";
    activeUser.end_at_m = time[1] || "None";
    this.setState({ activeUser });
  };

  handlePasswordChange = ({ target: { value } }) => {
    this.setState({ loginPassword: value });
  };

  handleLoginChange = ({ target: { value } }) => {
    this.setState({ loginUser: value });
  };

  handleCloseDialog = () => this.setState({ dialog: '', listPreview: {} });

  handleOpenDialog = (type: string) => this.setState({ dialog: type });

  handleInputChange = e => {
    const { value } = e.target;
    const id = e.target.getAttribute('id');
    const { activeUser } = this.state;

    activeUser[id] = value;
    this.setState({ activeUser });
  };

  renderDeleteConfirm = () => (
    <Dialog
      open={this.state.dialog === 'deleteConfirm'}
      fullWidth
      maxWidth="xs"
    >
      <DialogTitle>Vymazanie uctu</DialogTitle>
      <div className={styles.dialogcontent} />
      <DialogActions>
        <Button color="primary" autoFocus onClick={this.handleRemoveUser}>
          Potvrdit
        </Button>
        <Button onClick={this.handleCloseDialog} color="default">
          Zavriet
        </Button>
      </DialogActions>
    </Dialog>
  );

  handleImportFile = () => {
    const {
      activeUser: { username },
      dialog
    } = this.state

    if (this.state.listPreview.path) {
      console.error(this.state.listPreview.path)

      fs.unlink(`${username}_${dialog}.csv`, () =>
        fsExtra.copy(this.state.listPreview.path, `${username}_${dialog}.csv`)
          .then(() => this.setState({ listPreview: {} }))
          .catch(err => console.error(err)))

    }
  }

  removeCsvList = () => {
    const {
      activeUser: { username },
      dialog,
    } = this.state
    fs.unlink(`${username}_${dialog}.csv`, () => this.setState({ listPreview: {} }));
  }

  renderHistoryLog = () => {
    const {
      activeUser: { username },
      dialog
    } = this.state

    if (dialog === 'historyLog') {
      const data = fs.readFileSync(`${username}.log`, 'utf8');
      return (
        <Dialog open fullWidth maxWidth='xl'>
          <DialogTitle>{username} - aktivita</DialogTitle>
          <div className={styles.historycontent}>
            <Typography variant="body1" color="default">
              {data}
            </Typography>
          </div>
          <DialogActions>
            <Button onClick={this.handleCloseDialog} color='default'>
              OK
          </Button>
          </DialogActions>
        </Dialog>
      );
    }
  }

  renderListManagement = () => {
    const {
      activeUser: { username },
      dialog,
      listPreview: { path }
    } = this.state

    // fs.createReadStream(file ? file : `${username}_${dialog}.csv`, 'utf-8')
    //   .pipe(csv())
    //   .on('data', (row) => {
    //     if (this.state.listPreview.list.length <= 10) {
    //       const { username, id } = row;
    //       this.setState({ listPreview: { path: this.state.listPreview.path, list: [...this.state.listPreview.list, { username }] } });
    //     }
    //   });

    const listExists = !_.isNil(this.state.listPreview.stats)

    if (dialog == 'follow_list' || dialog == 'unfollow_whitelist') {
      if (_.isNil(this.state.listPreview.stats)) {
        fs.stat(`${username}_${dialog}.csv`, (err, stats) => {
          this.setState({ listPreview: { path: path, stats } })
        });
      }

      console.log(this.state.listPreview.stats)
      this.filesInput = null
      const handleChange = async (event) => {
        const { path } = event.target.files[0];
        console.log("path", path)
        await this.setState({ listPreview: { list: [], path: path }, })
      }

      return (
        <Dialog open fullWidth maxWidth='md'>
          <DialogActions className={styles.vertical}>
            <DialogTitle>{username} {dialog}</DialogTitle>
            <Typography variant="subtitle1">

              {listExists ? `Subor bol naposledy upraveny dna: ${this.state.listPreview.stats.mtime}` :
                "Vlozit novy subor"}
            </Typography>
            {!listExists && <Input
              type="file"
              ref={(input) => { this.filesInput = input }}
              name="file"
              icon='file text outline'
              iconPosition='left'
              label='Upload CSV'
              labelPosition='right'
              placeholder='UploadCSV...'
              onChange={handleChange}
            />}
            <div className={[styles.horizontal, styles.listActions]}>
              <Button disabled={!this.state.listPreview.path} onClick={() => this.handleImportFile()} color='primary' variant="outlined">
                Importovat novy zoznam
            </Button>
              <Button disabled={this.state.listPreview.path || !listExists} onClick={() => this.removeCsvList()} color='secondary' variant="outlined">
                Vymazat follow zoznam
            </Button>
              <Button onClick={this.handleCloseDialog} color='default' variant="outlined">
                Zavriet okno
            </Button></div>
          </DialogActions>

        </Dialog>
      )
    }
    return null
  }


  renderLogin = () => (
    <Dialog
      open={this.state.dialog === 'login'}
      fullWidth
      maxWidth="xs"
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Pridanie uctu</DialogTitle>
      <div className={styles.dialogcontent}>
        <TextField
          variant="outlined"
          label="Username"
          type="text"
          margin="normal"
          fullWidth
          value={this.state.loginUser}
          onChange={this.handleLoginChange}
        />
        <TextField
          variant="outlined"
          label="Heslo"
          margin="normal"
          type="password"
          fullWidth
          value={this.state.loginPassword}
          onChange={this.handlePasswordChange}
        />
      </div>
      <DialogActions>
        <Button color="primary" autoFocus onClick={this.handleAddUser} variant="outlined">
          Pridat
        </Button>
        <Button onClick={this.handleCloseDialog} color="default" variant="outlined">
          Zavriet
        </Button>
      </DialogActions>
    </Dialog>
  );

  renderAccounts = () => {
    const { activeUser, config, started } = this.state;
    const handleActiveUser = user => {
      this.setState({ activeUser: { ...config.DEFAULT, ...user } });
      this.updateConfig();
    };

    return _.map(_.filter(config, 'username'), (user, i) => (
      <MenuItem
        key={i}
        onClick={() => handleActiveUser(user)}
        selected={_.isEqual(activeUser.username, user.username)}
      >
        <ListItemText primary={user.username} />
        <ListItemIcon>
          {started[user.username] ? <CheckedIcon /> : <CloseIcon />}
        </ListItemIcon>
      </MenuItem>
    ));
  };

  render() {
    const { activeUser, started, editing } = this.state;
    const { handleStart, handleStop, handleOpenDialog } = this;
    const noActiveUser = _.isNil(activeUser.username)

    return (
      <div className={styles.page}>
        {this.renderLogin()}
        {this.renderHistoryLog()}
        {this.renderDeleteConfirm()}
        {this.renderListManagement()}
        <Divider className={styles.header} />
        <div data-tid="container" className={styles.wrapper}>
          <div className={styles.column}>
            <Button
              onClick={() => handleOpenDialog('login')}
              variant="outlined"
              color="default"
            >
              Pridat ucet
            </Button>
            <MenuList className={styles.accountlist}>
              {this.renderAccounts()}
            </MenuList>
          </div>
          <div className={styles.column}>
            <Button
              disabled={editing || noActiveUser}
              variant="contained"
              onClick={started[activeUser.username] ? handleStop : handleStart}
              color={started[activeUser.username] ? 'secondary' : 'primary'}
            >
              {started[activeUser.username] ? 'Zastavit' : 'Spustit bota'}
            </Button>
            <Button
              variant="outlined"
              disabled={noActiveUser}
              onClick={() => this.setState({ dialog: 'historyLog' })}
              color="primary"
            >
              Zobrazit log
            </Button>

            <div className={styles.account}>
              <Divider />
              <Typography variant="headline">
                {activeUser.username || '-'}
              </Typography>
              <Divider />
            </div>
            <Typography variant="subtitle1">
              Nastavenia
              </Typography>
            <List>
              <ListItem button disabled={noActiveUser} divider onClick={() => this.setState({ dialog: 'unfollow_whitelist' })}>
                <ListItemText primary="Unfollow whitelist" />
              </ListItem>
              <ListItem button disabled={noActiveUser} divider onClick={() => this.setState({ dialog: 'follow_list' })}>
                <ListItemText primary="Follow list" />
              </ListItem>
              <ListItem
                button
                disabled={noActiveUser || started[activeUser.username]}
                onClick={() => this.setState({ dialog: 'deleteConfirm' })}
              >
                <ListItemText primary="Vymazat ucet" />
              </ListItem>
            </List>
          </div>
          <div className={styles.column}>
            <Button
              variant="outlined"
              color="default"
              to={routes.COUNTER}
              disabled={noActiveUser || started[activeUser.username] || noActiveUser}
              onClick={this.handleConfigSave}
            >
              {started[activeUser.username] ? 'Ulozene parametre' : 'Ulozit parametre'}
            </Button>
            <div className={styles.configlist}>
              <TextField
                id="time"
                label="Cas spustenia"
                margin="normal"
                type="time"
                // defaultValue="00:00"
                disabled={noActiveUser || started[activeUser.username]}
                value={`${activeUser.start_at_h}:${activeUser.start_at_m}`}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{
                  step: 300, // 5 min
                }}
                onChange={this.handleStartTimeChange}
              />
              <TextField
                id="time"
                margin="normal"
                label="Cas ukoncenia"
                disabled={noActiveUser || started[activeUser.username]}
                type="time"
                // defaultValue="00:00"
                value={`${activeUser.end_at_h}:${activeUser.end_at_m}`}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{
                  step: 300, // 5 min
                }}
                onChange={this.handleEndTimeChange}
              />
              <TextField
                variant="outlined"
                type="number"
                margin="dense"
                label="Like / den"
                id="like_per_day"
                defaultValue={0}
                disabled={noActiveUser || started[activeUser.username]}
                value={activeUser.like_per_day}
                onChange={this.handleInputChange}
              />
              <TextField
                variant="outlined"
                type="dense"
                margin="normal"
                label="Follow / den"
                id="follow_per_day"
                defaultValue={0}
                disabled={noActiveUser || started[activeUser.username]}
                value={activeUser.follow_per_day}
                onChange={this.handleInputChange}
              />
              <TextField
                variant="outlined"
                label="Unfollow / den"
                type="dense"
                margin="normal"
                id="unfollow_per_day"
                defaultValue={0}
                disabled={noActiveUser || started[activeUser.username]}
                value={activeUser.unfollow_per_day}
                onChange={this.handleInputChange}
              />
              <TextField
                variant="outlined"
                label="Follow time (sec)"
                type="dense"
                margin="normal"
                id="follow_time"
                defaultValue={0}
                disabled={noActiveUser || started[activeUser.username]}
                value={activeUser.follow_time}
                onChange={this.handleInputChange}
              />
              <TextField
                label="Hashtags"
                multiline
                margin="dense"
                rows={5}
                variant="outlined"
                id="tag_list"
                defaultValue=" "
                disabled={noActiveUser || started[activeUser.username]}
                value={activeUser.tag_list}
                onChange={this.handleInputChange}
              />

            </div>
          </div>
        </div>
        <Divider className={styles.footer} />
      </div>
    );
  }
}
